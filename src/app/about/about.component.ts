import { Component, OnInit } from '@angular/core';
import { Observable, noop, of, concat, interval, merge } from 'rxjs';
import { map } from 'rxjs/operators';
import { Course } from '../model/course';
import { createHttpObservable } from '../common/util';

@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  constructor() { }

  ngOnInit() {
    const http$: Observable<Course[]> = createHttpObservable('api/courses');
    const sub = http$.subscribe(console.log);

    setTimeout(() => sub.unsubscribe(), 0);
  }
}
