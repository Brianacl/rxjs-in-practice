import {Component, OnInit} from '@angular/core';
import {Course} from "../model/course";
import {interval, Observable, of, timer, noop, throwError} from 'rxjs';
import {catchError, delayWhen, map, retryWhen, shareReplay, tap, finalize} from 'rxjs/operators';

import { createHttpObservable } from '../common/util';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    beginnersCourses$: Observable<Course[]>;
    advancedCourses$: Observable<Course[]>;

    constructor() {

    }

    ngOnInit() {
        const http$ = createHttpObservable('api/courses');

        const courses$: Observable<Course[]> = http$
            .pipe(
                tap(() => console.log('HTTP request executed')),
                map(res => Object.values(res['payload'])), // Get the payload of the response
                shareReplay(), // Shares the response along all the observable line
                retryWhen(errors => errors.pipe(
                    delayWhen(() => timer(2000))
                ))
            );

        this.beginnersCourses$ = courses$.pipe(
            map(courses => courses
                .filter(course => course.category === 'BEGINNER'))
        );

        this.advancedCourses$ = courses$.pipe(
            map(courses => courses
                .filter(course => course.category === 'ADVANCED'))
        );
    }
}
